<?php 
	include("conexão.php");
    $id = $_GET['id'];
    $linha=$infos_bd->prepare("SELECT * FROM artigo WHERE id = :id");
    $linha->bindValue(":id",$id);
    $linha->execute();
    $resultado = $linha->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    </head>
    <body>
    	<style>
            #fundo {
                background-color: #98f74a;
            }
        </style>

        <div class="container" id="fundo">
            <br>
            <form action="C_alterar_artigo.php" method='get'>
            <?php foreach ($resultado as $key => $value) {
                if ($key != 'id') {
            ?>
                <p><?php echo $key ?>:<br>
                <input name="<?php echo $key ?>" type="text" class="form-control" placeholder="<?php echo $value ?>" value="<?php echo $value ?>"></p>
            <?php 
            }
                else {
            ?>
                <p><?php echo $key ?>:<br>
                <input name="<?php echo $key ?>" type="text" class="form-control" placeholder="<?php echo $value ?>" value="<?php echo $value ?>" readonly></p>
            <?php
                }
            } ?>
            <br>
            <button type="submit" class="btn btn-outline-danger">
            Salvar
            </button>
            </form>
            <br>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>    

    </body>
</html>