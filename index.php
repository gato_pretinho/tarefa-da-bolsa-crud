<?php 
	include("conexão.php");
?>
<!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    </head>
    <body>
    	<style>
            #fundo {
                background-color: #98f74a;
            }
        </style>

        <div class="container" id="fundo">
        	<br>
        	<div class="row">
    			<div class="col-sm">
        		<?php

					$linhas = mysqli_query($MySQLi, 'SELECT * FROM artigo');
	    			while($artigo = mysqli_fetch_array($linhas)):

				?>

		    	<div class="card" style="width: 26rem;">
  
  					<div class="card-body">
    					<h5 class="card-title"><?php echo $artigo['nome'] ?></h5><br>
    					<p class="card-text"><?php echo $artigo['autor'] ?></p>
    					<p class="card-text"><?php echo $artigo['revista'] ?></p>
    					<a href="C_deletar_artigo.php?id=<?php echo $artigo['id'] ?>">Deletar</a><br>
    					<a href="V_alterar_artigo.php?id=<?php echo $artigo['id'] ?>">Editar</a>
  					</div>
				</div>
				<br>

				<?php endwhile ?>

				</div>
				<div class="col-sm">
					<h5>Inserir novo artigo:</h5>
					<br>
					<form action="C_inserir_artigo.php" method='get'>
						<p>Nome:<br>
						<input name="nome" type="text" class="form-control"></p>
						<p>Autor:<br>
						<input name="autor" type="text" class="form-control"></p>
						<p>Revista em que foi publicado:<br>
						<input name="revista" type="text" class="form-control"></p>
						<br>
						<button	type="submit" class="btn btn-outline-danger">
						Salvar
						</button>
					</form>

				</div>
			</div>
			<br>
		</div>
		
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    </body>
</html>
